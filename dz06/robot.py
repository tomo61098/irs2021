import numpy as np
from shared import R

cpr = 200

# wheel base / 2
l = 0.1
# wheel radius
r = 0.05
# robot length
d = 0.25
# wheel width
wheel_w = 0.03
# wheel L
alpha_l = np.pi / 2
beta_l = 0
# wheel R
alpha_r = -np.pi / 2
beta_r = np.pi

# npr. maksimalna kutna brzina kotaca je 200 okr/min
phi_max = 200 * 2 * np.pi / 60

wheel_points = [
    np.array([-wheel_w / 2, -r, 0]),
    np.array([0, -r - 0.01, 0]),
    np.array([wheel_w / 2, -r, 0]),
    np.array([wheel_w / 2, r, 0]),
    np.array([-wheel_w / 2, r, 0])
]

chassis_point = [
    np.array([-0.07, -l, 0]),
    np.array([d, -l, 0]),
    np.array([d, l, 0]),
    np.array([-0.07, l, 0]),
]

wheel_points_m = np.array(wheel_points)
chassis_point_m = np.array(chassis_point)

s_dist_max = 0.5
s_dist_min = 0.05
s_alpha = np.deg2rad(20)

from rectangle import Rectangle
from polygon import Polygon

class Robot:
    def __init__(self, i_xi=np.array([0, 0, 0], dtype=float)):
        self.i_xi = i_xi

    @property
    def wheel_l(self):
        # prvo zarotirati točku kotača u njegovom kordinatnom sustavu za beta
        # onda translatirati za l, i onda zarotirati za alpha u kordinatnom sustavu robota
        # onda zarotirati za njegovu rotaciju (i_xi[2], odn theta) i onda translatirati s obzirom na globalni kordinatni
        return np.dot(np.dot(np.dot(wheel_points_m, R(beta_l)) + np.array([l, 0, 0]), R(alpha_l)),
                         R(self.i_xi[2])) + np.array([self.i_xi[0], self.i_xi[1], 0])

    @property
    def wheel_r(self):
        return np.dot(np.dot(np.dot(wheel_points_m, R(beta_r)) + np.array([l, 0, 0]), R(alpha_r)), R(self.i_xi[2])) + np.array([self.i_xi[0], self.i_xi[1], 0])

    @property
    def chassis(self):
        # šasija je malo lakša jer je u koordinatnom sustavu robota, dakle w.r.t. na koordinatni sustav robota, ne radimo transformaciju
        return np.dot(chassis_point_m, R(self.i_xi[2])) + np.array([self.i_xi[0], self.i_xi[1], 0])

    def r_inverse_kinematics(self, r_v_dot, r_omega):
        phi_dots = 1 / r * np.array([
            [1, 0, -l],
            [1, 0, l],
            [0, 1, 0]
        ], dtype=float) @ np.array([
            r_v_dot, 0, r_omega
        ], dtype=float)

        phi_dot_l = phi_dots[0]
        phi_dot_r = phi_dots[1]
        return phi_dot_l, phi_dot_r

    def inverse_kinematics(self, v_dot, omega):
        # pretvoriti transl. brzinu v i kutnu brzinu omega
        # u x_dot, y_dot i omega, i onda to pretvoriti preko
        # invezne kinematike u phi_dots
        phi_dots = 1 / r * np.array([
            [1, 0, -l],
            [1, 0, l],
            [0, 1, 0]
        ], dtype=float) @ R(self.i_xi[2]) @ np.array([
            v_dot * np.cos(self.i_xi[2]),
            v_dot * np.sin(self.i_xi[2]),
            omega
        ], dtype=float)

        phi_dot_l = phi_dots[0]
        phi_dot_r = phi_dots[1]
        return phi_dot_l, phi_dot_r

    def forward_kinematics(self, phi_dot_l, phi_dot_r):
        return R(self.i_xi[2]).transpose() @ np.array(
            [[1 / 2, 1 / 2, 0], [0, 0, 1], [-1 / 2 / l, 1 / 2 / l, 0]]) @ np.array([r * phi_dot_l, r * phi_dot_r, 0])
    
    def update_state(self, i_xi_dot, dt):
        self.i_xi += i_xi_dot * dt

    def update_state_r(self, r_v_dot, r_omega, dt):
        self.update_state(self.forward_kinematics(*self.r_inverse_kinematics(r_v_dot, r_omega)), dt)

    def read_encoders(self, phi_dot_l, phi_dot_r, dt):
        delta_c_l = int(cpr * phi_dot_l * dt / 2 / np.pi)
        delta_c_r = int(cpr * phi_dot_r * dt / 2 / np.pi)
        return delta_c_l, delta_c_r

    def encoder_deltas_to_phis(self, delta_c_l, delta_c_r, dt):
        phi_dot_l_enc = delta_c_l * 2 * np.pi / cpr / dt
        phi_dot_r_enc = delta_c_r * 2 * np.pi / cpr / dt
        return phi_dot_l_enc, phi_dot_r_enc
