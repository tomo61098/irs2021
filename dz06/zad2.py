import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as stat

z_hit = .75
z_short = .25
z_max = .25
z_rand = .0

sigma_hit = .25
lambda_short = 2.3
s_max = 5
s_eps = .05

M = 10000

def genTS(sigma_hit, lambda_short):
    (a, b) = (0 - 3) / sigma_hit, (s_max - 3) / sigma_hit
    Tnorm = stat.truncnorm(a, b, loc = 3, scale = sigma_hit)
    Texpo = stat.truncexpon(3 * lambda_short, loc = 0, scale = 1 / lambda_short)
    return (Tnorm, Texpo)

(Tnorm, Texpo) = genTS(sigma_hit, lambda_short)

def prob(z_t, Tnorm = Tnorm, Texpo = Texpo):
    # case 1
    p_hit = Tnorm.pdf(z_t)
    
    # case 2
    p_short = Texpo.pdf(z_t)

    return z_hit * p_hit + z_short * p_short

def sample(n, Tnorm = Tnorm, Texpo = Texpo):
    B = np.random.binomial(1, z_hit, n)

    hits = Tnorm.rvs(n)
    shorts = Texpo.rvs(n)

    return B * hits + (1 - B) * shorts

def learn(Z, sigma_hit0 = .1, lambda_short0 = 1):
    m = Z.shape[0]
    print(m)
    for _ in range(10):
        (Tnorm, Texpo) = genTS(sigma_hit0, lambda_short0)
        ehit = Tnorm.pdf(Z)
        eshort = Texpo.pdf(Z)
        n = ehit + eshort
        ehit /= n
        eshort /= n

        z_hit = ehit.sum() / m
        z_short = eshort.sum() / m
        
        sigma_hit0 = np.sqrt(np.sum(ehit * (Z - 3) ** 2) / z_hit / m)
        lambda_short0 = z_short * m / np.sum(Z * eshort)
        
    return (z_hit, z_short, sigma_hit0, lambda_short0)

if __name__ == '__main__':
    z_hit, z_short
    G = np.linspace(-1, 5, M)
    A = prob(G)
    sm = sample(M)
    
    (z_hit, z_short, sigma_hit, lambda_short) = learn(sm)
    print(z_hit, z_short, sigma_hit, lambda_short)
    B = prob(G, *genTS(sigma_hit, lambda_short))
        
    plt.plot(G, A, '--')
    plt.plot(G, B, '-.')
    plt.hist(sm, bins = 50, density = True, color = 'magenta')
    plt.show()
