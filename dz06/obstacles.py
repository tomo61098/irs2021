import numpy as np
from shared import R

o1_obstacle1_points = np.array([
    [-0.25, -0.25, 0],
    [0.25, -0.25, 0],
    [0.25, 0.25, 0],
    [-0.25, 0.25, 0]
], dtype=float)

i_R_o1 = R(np.pi / 2.2).transpose()
i_t_o1 = np.array([0.75, 0.5, 0], dtype=float)


o2_obstacle2_points = np.array([
    [-0.25, -0.25, 0],
    [0.25, -0.25, 0],
    [0.25, 0.25, 0],
    [-0.25, 0.25, 0]
], dtype=float)

i_R_o2 = R(np.pi / 4).transpose()
i_t_o2 = np.array([-1, 1, 0], dtype=float)

def generate_ngon(n: int):
    return np.array([
        [np.cos(2 * np.pi * i / n), np.sin(2*np.pi*i/n), 0] for i in range(n)])

o3_obstacle2_points = generate_ngon(5) / 5

i_R_o3 = R(np.pi/3).T
i_t_o3 = np.array([ -1.2, .25, 0 ])

o4_obstacle2_points = generate_ngon(7) / 5

i_R_o4 = R(np.pi/4).T
i_t_o4 = np.array([ -.55, 1.25, 0 ])

o5_obstacle2_points = generate_ngon(5) / 1.5

i_R_o5 = np.eye(3)
i_t_o5 = np.array([ 0, 0, 0. ])

o6_obstacle2_points = generate_ngon(5)

i_R_o6 = R(np.pi/2)
i_t_o6 = np.array([ 1, 1, 0. ])

o7_obstacle2_points = generate_ngon(100)
i_R_o7 = np.eye(3)
i_t_o7 = np.array([ -1.5, .5, 0 ])

from polygon import Polygon
from quadtree import QuadTree


class Obstacle:
    def __init__(self, points, R, t):
        self.points = points
        self.R = R
        self.t = t

        self.polygon = Polygon(self[:, :2])

    def __getitem__(self, item):
        return (np.dot(self.points, self.R.transpose()) + self.t)[item]

    def get_bounding_rectangle(self):
        x_min = np.min(self[:, 0])
        x_max = np.max(self[:, 0])
        y_min = np.min(self[:, 1])
        y_max = np.max(self[:, 1])
        return x_min, y_min, x_max - x_min, y_max - y_min


if __name__ == "__main__":
    o = Obstacle(o1_obstacle1_points, i_R_o1, i_t_o1)
    q = QuadTree([o])
