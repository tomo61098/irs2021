import numpy as np


def R(theta):
    return np.array([
        [np.cos(theta), np.sin(theta), 0],
        [-np.sin(theta), np.cos(theta), 0],
        [0, 0, 1]
    ], dtype=float)


def r_R_i_array(thetas):
    M = thetas.shape[0]
    R_arr = np.zeros((M, 3, 3), dtype=float)
    R_arr[:, 0, 0] = np.cos(thetas)
    R_arr[:, 0, 1] = np.sin(thetas)
    R_arr[:, 1, 0] = -np.sin(thetas)
    R_arr[:, 1, 1] = np.cos(thetas)
    R_arr[:, 2, 2] = np.ones(M, dtype=float)
    return R_arr


def i_R_r_array(thetas):
    M = thetas.shape[0]
    R_arr = np.zeros((M, 3, 3), dtype=float)
    R_arr[:, 0, 0] = np.cos(thetas)
    R_arr[:, 0, 1] = -np.sin(thetas)
    R_arr[:, 1, 0] = np.sin(thetas)
    R_arr[:, 1, 1] = np.cos(thetas)
    R_arr[:, 2, 2] = np.ones(M, dtype=float)
    return R_arr