import numpy as np
from shared import *
from sampler import sampler

def sample_normal_distribution(b_squared, M):
    b = np.sqrt(b_squared)
    return 1 / 2 * np.sum(np.random.uniform(-b, b, (M, 12)), axis=1)

alpha_1 = alpha_2 = alpha_3 = alpha_4 = alpha_5 = alpha_6 = 1.1

def sample_motion_model_odometry(u_current, x_prev):
    M = x_prev.shape[0]
    # u_current: 0. redak - prethodno stanje, 1. redak - trenutno
    delta_rot_1 = np.arctan2(
        u_current[1, 1] - u_current[0, 1],
        u_current[1, 0] - u_current[0, 0],
    ) - u_current[0, 2]
    delta_trans = np.sqrt(
        (u_current[1, 0] - u_current[0, 0]) ** 2 + (u_current[1, 1] - u_current[0, 1]) ** 2
    )
    delta_rot_2 = u_current[1, 2] - u_current[0, 2] - delta_rot_1

    # dodatno
    delta_rot_1 = np.arctan2(np.sin(delta_rot_1), np.cos(delta_rot_1))
    delta_rot_2 = np.arctan2(np.sin(delta_rot_2), np.cos(delta_rot_2))

    delta_hat_rot_1 = delta_rot_1 - sample_normal_distribution(
        alpha_1 * delta_rot_1 ** 2 + alpha_2 * delta_trans ** 2, M
    )
    delta_hat_trans = delta_trans - sample_normal_distribution(
        alpha_3 * delta_trans ** 2 + alpha_4 * delta_rot_1 ** 2 + alpha_4 * delta_rot_2 ** 2, M
    )
    delta_hat_rot_2 = delta_rot_2 - sample_normal_distribution(
        alpha_1 * delta_rot_2 ** 2 + alpha_2 * delta_trans ** 2, M
    )

    x = x_prev[:, 0]
    y = x_prev[:, 1]
    theta = x_prev[:, 2]

    x_current = x + delta_hat_trans * np.cos(theta + delta_hat_rot_1)
    y_current = y + delta_hat_trans * np.sin(theta + delta_hat_rot_1)
    theta_current = theta + delta_hat_rot_1 + delta_hat_rot_2

    return np.vstack((x_current, y_current, theta_current)).T

def prediction(particles, r_v_dot, r_omega, dt, var):
    r_vel = np.array([ r_v_dot, 0, r_omega ], dtype = float)
    particles += np.tensordot(
            r_vel,
            r_R_i_array(particles[:, 2]),
            axes = (0, 1)
        ) * dt

    particles += np.random.normal(0, var, particles.shape)
    particles[:, 2] = np.arctan2(np.sin(particles[:, 2]), np.cos(particles[:, 2]))
    return particles

def correction(robot, landmarks, particles, var):
    M = len(particles)
    landmarks_phi = np.arctan2(landmarks[:, 1], landmarks[:, 0])

    m_res = landmarks - robot.i_xi[:2]
    m_res += np.random.normal(0, var, (landmarks.shape[0], 2))
    phi_res = landmarks_phi - robot.i_xi[2]
    phi_res += np.random.normal(0, var, landmarks.shape[0])
        
    particles_reshaped = particles[:, None]
    p_m_res = landmarks - particles_reshaped[:, :, :2]
    p_m_res += np.random.normal(0, var, (M, landmarks.shape[0], 2))
    p_phi_res = landmarks_phi - particles_reshaped[:, :, 2]
    p_phi_res += np.random.normal(0, var, (M, landmarks.shape[0]))

    diff_pos_r_p = np.linalg.norm(m_res - p_m_res, axis=(1, 2))
    diff_or_r_p = np.linalg.norm(phi_res - p_phi_res, axis=1)

    diff = diff_pos_r_p + diff_or_r_p
    x_0 = y_1 = np.min(diff)
    x_1 = y_0 = np.max(diff)
    sim = (y_1 - y_0) / (x_1 - x_0) * (diff - x_0) + y_0
    sim /= np.sum(sim)
    weights = sim

    mean = 1 / M * np.sum(particles, axis=0)
    mean[2] = np.arctan2(np.sin(mean[2]), np.cos(mean[2]))
    
    indices = sampler(weights)
    particles = particles[indices]
    return mean, particles
