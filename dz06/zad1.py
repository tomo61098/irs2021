import numpy as np
import matplotlib.pyplot as plt

from shared import R, i_R_r_array, r_R_i_array
from sampler import sampler
from robot import Robot
from plot1 import Plot
from particle_filter import *

if __name__ == "__main__":

    dt = 0.01
    r_v_dot = 0.6
    r_omega = np.pi

    r = Robot(np.array([1.7, 1.5, 4 / 3 * np.pi]))

    landmarks = np.random.normal(0, 1, (7, 2))
    landmarks_phi = np.arctan2(landmarks[:, 1], landmarks[:, 0])


    M = 1000
    particles = np.random.randn(M, 3)
    particles[:, 2] = np.arctan2(np.sin(particles[:, 2]), np.cos(particles[:, 2]))

    plt.show()

    var = 0.01

    mean = 1 / M * np.sum(particles, axis=0)
    mean[2] = np.arctan2(np.sin(mean[2]), np.cos(mean[2]))
    er = Robot(mean)

    plot = Plot(r, er, landmarks, particles)

    for i in range(1000):
        phi_dot_l, phi_dot_r = r.r_inverse_kinematics(r_v_dot, r_omega)
        r.update_state_r(r_v_dot, r_omega, dt)

        delta_c_l, delta_c_r = r.read_encoders(phi_dot_l, phi_dot_r, dt)
        phi_dot_l_enc, phi_dot_r_enc = r.encoder_deltas_to_phis(delta_c_l, delta_c_r, dt)
        i_xi_dot_enc = r.forward_kinematics(phi_dot_l_enc, phi_dot_r_enc)

        i_xi_prev = er.i_xi.copy()
        er.update_state(i_xi_dot_enc, dt)
        i_xi = er.i_xi.copy()

        particles[:] = sample_motion_model_odometry(np.vstack([i_xi_prev, i_xi]), particles)
        #particles[:] = prediction(particles, r_v_dot, r_omega, dt, var)
        [er.i_xi, particles[:]] = correction(r, landmarks, particles, var)

        plot.update()

        if i % 100 == 0:
            r_omega *= -1

        plt.pause(dt)
