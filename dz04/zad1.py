from plot import Plot, plt
from robot import Robot, np, l, phi_max # phi_max = 200 * 2 * np.pi / 60

class Goal:
    def __init__(self, i_g=np.zeros(3, dtype=float)):
        self.i_g = i_g

from pid import PID

class GoToGoalController:
    def __init__(self, robot, goal):
        self.goal = goal
        self.robot = robot
        self.pid = PID()
        
    def __call__(self, dt):
        e = np.arctan2(self.goal.i_g[1] - self.robot.i_xi[1], self.goal.i_g[0] - self.robot.i_xi[0]) - self.robot.i_xi[2]
        e = np.arctan2(np.sin(e), np.cos(e))

        r_omega = self.pid(e, dt)
        r_v = 1

        r_v_feas = r_v
        r_omega_feas = r_omega

        (pl, pr) = self.robot.r_inverse_kinematics(r_v, r_omega)
        pmx = max(pl, pr)
        if pmx > phi_max: # global in robot.py
            pdt = pmx - phi_max
            pl -= pdt
            pr -= pdt
            pl = max(0, pl)
            pr = max(0, pr)
        
        [x, y, omega] = self.robot.forward_kinematics(pl, pr)
        r_omega_feas = omega
        r_v_feas = np.hypot(x, y)
        
        return r_v_feas, r_omega_feas

class StopController:
    def __call__(self, dt):
        return 0, 0

if __name__ == "__main__":
    robot = Robot()
    goal = Goal(np.array([-1, 1, 0], dtype=float))
    plot = Plot(robot, goal, [])
    dt = 0.010
    plt.draw()



    gtgc = GoToGoalController(robot, goal)
    sc = StopController()

    ac = gtgc

    t = 0
    while True:
        t += dt

        if np.linalg.norm(robot.i_xi[:2] - goal.i_g[:2]) < l:
            ac = sc
        else:
            ac = gtgc

        r_v_dot, r_omega = ac(dt)
        phi_dot_l, phi_dot_r = robot.r_inverse_kinematics(r_v_dot, r_omega)
        i_xi_dot = robot.forward_kinematics(phi_dot_l, phi_dot_r)
        robot.update_state(i_xi_dot, dt)
        plot.update()
        plt.pause(dt)

    plt.waitforbuttonpress()
