import numpy as np
from pid import PID
from robot import s_dist_max, phi_max
from shared import R

class GoToGoalController:
    def __init__(self, robot, goal):
        self.goal = goal
        self.robot = robot
        self.pid = PID()

    def __call__(self, dt):
        e = np.arctan2(self.goal.i_g[1] - self.robot.i_xi[1], self.goal.i_g[0] - self.robot.i_xi[0]) - self.robot.i_xi[2]
        e = np.arctan2(np.sin(e), np.cos(e))

        r_omega = self.pid(e, dt)
        r_v_dot = 1

        r_v_dot_feas = r_v_dot
        r_omega_feas = r_omega
        
        sgn_omega = np.sign(r_omega)
        if sgn_omega == 0: sgn_omega = -1.
        
        r_omega = abs(r_omega)

        (pl, pr) = self.robot.r_inverse_kinematics(r_v_dot, r_omega)
        pmx = max(pl, pr)
        if pmx > phi_max: # feasible phi
            pdt = pmx - phi_max
            pl -= pdt
            pr -= pdt
            pl = max(0, pl)
            pr = max(0, pr)
        
        [x, y, omega] = self.robot.forward_kinematics(pl, pr)
        r_omega_feas = sgn_omega * omega
        r_v_feas = np.hypot(x, y)

        return r_v_feas, r_omega_feas

    def reset_pid(self):
        self.pid = PID()

class StopController:
    def __call__(self, dt):
        return 0, 0

class AvoidObstacleController:
    def __init__(self, robot):
        self.robot = robot
        self.pid = PID()

    def reset_pid(self):
        self.pid = PID()

    def __call__(self, dt):
        dts = map(lambda x: x.d, self.robot.sensors_array)
        dts = np.subtract(s_dist_max, list(dts)) / s_dist_max
        vec = map(lambda x: x.r_s[2], self.robot.sensors_array)
        vec = np.pi + np.array(list(vec)) # in opposite direction of sensors
        vec = np.array([ [np.cos(i), np.sin(i)] for i in vec ])
        vec = vec * dts[:, None] # (d_max - d)/d_max
        vec = vec.sum(0) # sum of sensor vectors strategy #3

        '''
        import matplotlib.pyplot as plt
        
        plt.plot(np.cos(vec), np.sin(vec), '.')
        plt.show()
        '''

        e = np.arctan2(vec[1], vec[0])
        e = np.arctan2(np.sin(e), np.cos(e))
        
        r_omega = self.pid(e, dt)
        r_v_dot = 1
        
        r_v_dot_feas = r_v_dot
        r_omega_feas = r_omega
        sgn_omega = np.sign(r_omega)
        if sgn_omega == 0: sgn_omega = -1.
        r_omega = abs(r_omega)
        
        (pl, pr) = self.robot.r_inverse_kinematics(r_v_dot, r_omega)
        pmx = max(pl, pr)
        if pmx > phi_max: # feasible phi
            pdt = pmx - phi_max
            pl -= pdt
            pr -= pdt
            pl = max(0, pl)
            pr = max(0, pr)
        
        [x, y, omega] = self.robot.forward_kinematics(pl, pr)
        r_omega_feas = sgn_omega * omega
        r_v_feas = np.hypot(x, y)

        return r_v_feas, r_omega_feas
