import numpy as np
import matplotlib.pyplot as plt
from scipy.spatial.transform import Rotation

def RR(w: np.array) -> np.array:
    return Rotation.from_rotvec(w).as_matrix()

if __name__ == '__main__':
    plt.ion()
    fig = plt.figure()
    ax = fig.add_subplot(111, projection = '3d')
    ax.set_xlim(-5, 5)
    ax.set_ylim(-5, 5)
    ax.set_zlim(-5, 5)

    dt = 0.01

    pts1 = np.eye(4, 3)
    pts2 = pts1.copy()
    
    sc1 = ax.scatter(*pts1.T)
    sc2 = ax.scatter(*pts2.T)
    plt.show()

    w01 = np.zeros(3)
    p01 = np.array([ 1., 1., 0. ])
    pd01 = np.zeros((3,1))
    R01 = np.eye(3)

    w12 = np.array([ np.pi, -np.pi, 0 ])
    p12 = np.array([ 2., 0., 2. ])
    pd12 = np.zeros((3,1))
    R12 = np.eye(3)
    
    for _ in range(100):
        R01 = (RR(w01*dt)) @ R01
        R12 = (RR(w12*dt)) @ R12
        pd01 += p01[:,None] * dt
        pd12 += p12[:,None] * dt

        sc1._offsets3d = R01 @ pts1.T + pd01
        sc2._offsets3d = R01 @ (R12 @ pts2.T + pd12) + pd01
        fig.canvas.draw_idle()

        plt.pause(dt)
    
    plt.close()

    ########
    fig = plt.figure()
    ax = fig.add_subplot(111, projection = '3d')
    ax.set_xlim(-5, 5)
    ax.set_ylim(-5, 5)
    ax.set_zlim(-5, 5)
    
    dt = 0.01

    pts1 = np.eye(4, 3)
    pts2 = pts1.copy()
    
    sc1 = ax.scatter(*pts1.T)
    sc2 = ax.scatter(*pts2.T)
    plt.show()

    w01 = np.array([ 2*np.pi, 0., 0. ])
    p01 = np.array([ 1., 1., 0. ])
    pd01 = np.zeros((3,1))
    R01 = np.eye(3)

    w12 = np.array([ 0., 8*np.pi, 0. ])
    p12 = np.array([ -2., -3., 0. ])
    pd12 = np.zeros((3,1))
    R12 = np.eye(3)
    
    for _ in range(100):
        R01 = (RR(w01*dt)) @ R01
        R12 = (RR(w12*dt)) @ R12
        pd01 += p01[:,None] * dt
        pd12 += p12[:,None] * dt

        sc1._offsets3d = R01 @ pts1.T + pd01
        sc2._offsets3d = R01 @ (R12 @ pts2.T + pd12) + pd01
        fig.canvas.draw_idle()

        plt.pause(dt)
    
    plt.close()
