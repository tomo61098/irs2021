import numpy as np
import matplotlib.pyplot as plt
from scipy.spatial.transform import Rotation

def RR(w: np.array) -> np.array:
    return Rotation.from_rotvec(w).as_matrix()

if __name__ == '__main__':
    plt.ion()
    fig = plt.figure()
    ax = fig.add_subplot(111, projection = '3d')
    ax.set_xlim(-5, 5)
    ax.set_ylim(-5, 5)
    ax.set_zlim(-5, 5)

    w = np.array([ 0, np.pi/4, 0 ])
    j_R_i = np.eye(3)
    dt = 0.01

    point_init = np.eye(4, 3)
    sc = ax.scatter(*point_init.T)
    plt.show()
    
    for _ in range(100):
        j_R_i = (RR(w*dt)) @ j_R_i
        sc._offsets3d = j_R_i @ point_init.T
        fig.canvas.draw_idle()

        plt.pause(dt)
    
    w = np.array([ np.pi, 0, 0 ])
    for _ in range(100):
        j_R_i = (RR(w*dt)) @ j_R_i
        sc._offsets3d = j_R_i @ point_init.T
        fig.canvas.draw_idle()

        plt.pause(dt)
    
    plt.close()
