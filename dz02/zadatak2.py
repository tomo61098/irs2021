import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Polygon
from scipy.spatial.transform import Rotation

def init_plot(r_from = -1, r_to = 1):
    plt.ion()
    fig = plt.figure(figsize = (8, 8))
    ax = fig.add_subplot(111)

    ax.set_xlim(r_from, r_to)
    ax.set_ylim(r_from, r_to)
    ax.set_aspect("equal")

    plt.grid()
    ax.set_xlabel("X")
    ax.set_ylabel("Y")

    return (fig, ax)

def R(theta):
    return Rotation.from_rotvec([ 0, 0, theta ]).as_matrix()

def J(a, b):
    return np.array([ np.sin(a + b), -np.cos(a + b), -0.32 * np.cos(b) ])

def C(a, b):
    return np.array([ np.cos(a + b), np.sin(a + b), 0.1 + 0.32 * np.sin(b) ])

INV = np.linalg.pinv

class Wheel(object):

    def __init__(self, d, r, ax):
        self.d = d
        self.r = r
        self.init_points = np.array([
            [ 0, 0, 1 ],
            [ 0, -d, 1 ],
            [ -r/4 , -d+r/2, 1 ],
            [ -r/4, -d-r/2, 1 ],
            [ r/4, -d-r/2, 1 ],
            [ r/4, -d+r/2, 1 ]
        ])

        self.circle = ax.plot([0], [0], '.k')[0]
        self.line = ax.plot([0, 0], [0, -d], 'k')[0]
        self.wheel = ax.add_patch(Polygon(self.init_points[2:, :2], color = 'k'))
        
    def apply(self, T):
        self.points = self.init_points @ T.T
    
    def draw(self):
        self.circle.set_data(self.points[0][0], self.points[0][1])
        self.line.set_data(self.points[:2, 0], self.points[:2, 1])
        self.wheel.xy = self.points[2:, :2]

class Car(object):

    def __init__(self, ax):
    
        self.x = 0
        self.y = 0
        self.theta = 0
    
        l = 0.32
        self.init_chasis = np.array([
            [ -l, -0.18809128073359138, 1 ],
            [ .25888543819998316, -0.18809128073359138, 1 ],
            [ .25888543819998316, 0.18809128073359138, 1 ],
            [ -l, 0.18809128073359138, 1 ]
        ])
        
        self.chasis = ax.add_patch(Polygon(self.init_chasis[:, :2], color = 'c'))
        self.wheels = [
            Wheel(.1, .05, ax),
            Wheel(.1, .05, ax),
            Wheel(.1, .05, ax)
        ]

        p5 = np.pi/5
        a = np.array([ p5, -p5, np.pi ])
        b = np.array([ -p5, p5 + 13*np.pi/10, 0 ])
        self.alphas = a.copy()
        self.betas = b.copy()

        self.apply()

        #self.dt = 0.01
        
        self.D = np.diag([ 0.05, 0.05, 0.05, -0.1, -0.1, -0.1 ])
        self.L = [ J(i,j) for (i,j) in zip(a, b) ]
        self.L.extend([ C(i, j) for (i,j) in zip(a, b) ])
        self.L = np.array(self.L)

    def apply(self):
        self.L = [ J(i,j) for (i, j) in zip(self.alphas, self.betas) ]
        self.L.extend([ C(i, j) for (i,j) in zip(self.alphas, self.betas) ])
        self.L = np.array(self.L)

        
        T1 = R(self.theta)
        T1[0, 2] = self.x
        T1[1, 2] = self.y
        self.points = self.init_chasis @ T1.T
        
        WT = [ R(bi) for bi in self.betas ]
        for i in WT:
            i[0, 2] = 0.32
        
        WT = [ T1 @ R(ai) @ i for (ai, i) in zip(self.alphas, WT) ]
        for (i, j) in zip(self.wheels, WT):
            i.apply(j)
    
    def draw(self):
        self.chasis.xy = self.points[:, :2]
        for i in self.wheels:
            i.draw()
    
    def step(self, v, omega, dt = 0.01):
        theta = omega
        x = np.cos(theta) * v
        y = np.sin(theta) * v

        pb = INV(self.D, hermitian = True) @ self.L @ [ x, y, theta ]
        
        self.betas += pb[3:] * dt

        [x, y, theta] = R(self.theta) @ INV(self.L) @ self.D @ pb
        self.x += x * dt
        self.y += y * dt
        self.theta += theta * dt

        self.apply()
        self.draw()
        

if __name__ == '__main__':
    (fig, ax) = init_plot(-1, 2)
    plt.draw()

    cr = Car(ax)

    dt = 0.01
    
    for _ in range(100):

        cr.step(v = 1, omega = np.pi/4, dt = dt)
        
        fig.canvas.draw_idle()
        plt.pause(dt)

    for _ in range(100):

        cr.step(v = .3, omega = 0, dt = dt)
        
        fig.canvas.draw_idle()
        plt.pause(dt)

    for _ in range(100):

        cr.step(v = 1, omega = np.pi/2, dt = dt)
        
        fig.canvas.draw_idle()
        plt.pause(dt)

    for _ in range(100):

        cr.step(v = -1, omega = 0, dt = dt)
        
        fig.canvas.draw_idle()
        plt.pause(dt)
