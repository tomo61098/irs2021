import numpy as np
import matplotlib.pyplot as plt

plt.ion()
fig = plt.figure(figsize=(8, 8))

ax = fig.add_subplot(111, projection="3d")
ax.set_xlim(-1, 1)
ax.set_ylim(-1, 1)
ax.set_zlim(-1, 1)
ax.set_xlabel('X')
ax.set_ylabel('Y')
ax.set_zlabel('Z')


def DH(params):
    a = params[0]
    alpha = params[1]
    d = params[2]
    theta = params[3]
    return np.array([
        [np.cos(theta), -np.sin(theta) * np.cos(alpha), np.sin(theta) * np.sin(alpha), a * np.cos(theta)],
        [np.sin(theta), np.cos(theta) * np.cos(alpha), -np.cos(theta) * np.sin(alpha), a * np.sin(theta)],
        [0, np.sin(alpha), np.cos(alpha), d],
        [0, 0, 0, 1]
    ], dtype=float)


def update_plot():
    global axes_list
    global lines_list

    frame_last = None
    for i in range(manipulator_params.shape[0]):
        T_i = np.eye(4, dtype=float)
        for j in range(i + 1):
            T_i = T_i @ DH(manipulator_params[j])
        frame_i = frame.dot(T_i.transpose())

        if 0 < i < manipulator_params.shape[0] - 1:
            axes_list[i]._offsets3d = [frame_i[0, 0]], [frame_i[0, 1]], [frame_i[0, 2]]
        else:
            axes_list[i]._offsets3d = frame_i[:, 0], frame_i[:, 1], frame_i[:, 2]

        if i > 0:
            lines_list[i-1].set_data_3d([frame_last[0, 0], frame_i[0, 0]], [frame_last[0, 1], frame_i[0, 1]], [frame_last[0, 2], frame_i[0, 2]])
        frame_last = frame_i
    fig.canvas.draw_idle()
    plt.pause(dt)



if __name__ == "__main__":
    #okvir
    frame = np.array([
        [0, 0, 0, 1],
        [1, 0, 0, 1],
        [0, 1, 0, 1],
        [0, 0, 1, 1]
    ], dtype=float)
    # scale-at ću osi na 20 cm
    # da možemo pratiti što se događa
    frame[:, :3] = frame[:, :3] * 0.2

    # STANFORD Manipulator
    manipulator_params = np.load("./stanford_params.npy")
    #print(manipulator_params)
    joint_infos = np.load("./stanford_joints.npy")
    #print(joint_infos)
    
    colors = np.load("./stanford_colors.npy")
    joint_labels = np.load("./stanford_labels.npy")
    #print(joint_labels)

    dt = 0.01


    axes_list = []
    lines_list = []

    frame_last = None

    # 1. OVO SU FRAMEOVI SVAKE POVEZNICE
    for i in range(manipulator_params.shape[0]):
        T_i = np.eye(4, dtype=float)
        for j in range(i + 1):
            T_i = T_i @ DH(manipulator_params[j])
        frame_i = frame.dot(T_i.transpose())
        if i == manipulator_params.shape[0] - 1:
            T_e = T_i

        if 0 < i < manipulator_params.shape[0] - 1:
            axes_list.append(ax.scatter(frame_i[0, 0], frame_i[0, 1], frame_i[0, 2], color=colors[i], marker="."))
        else:
            axes_list.append(ax.scatter(frame_i[:, 0], frame_i[:, 1], frame_i[:, 2], color=colors[i], marker="."))

        # 2. LINIJE KOJE POVEZUJU (i - 1). i i. poveznicu
        if i > 0:
            line = ax.plot([frame_last[0, 0], frame_i[0, 0]], [frame_last[0, 1], frame_i[0, 1]], [frame_last[0, 2], frame_i[0, 2]], color=colors[i], linewidth=4, solid_capstyle="round")[0]
            lines_list.append(line)
        frame_last = frame_i



    T_e = np.eye(4, dtype=float)
    for params in manipulator_params:
        T_e = T_e @ DH(params)


    T_g = np.load('./stanford_goal.npy')

    frame_g = frame.dot(T_g.T)
    ax.scatter(frame_g[:, 0], frame_g[:, 1], frame_g[:, 2], color="#ff1744", marker="*")
    q_dot_0 = (np.random.rand(manipulator_params.shape[0]) - .5) * np.pi
    
    # 1. cartesian control
    while True:
        # 0_T_g, 0_T_e
        T_e = np.eye(4, dtype=float)
        for params in manipulator_params:
            T_e = T_e @ DH(params)

        e_T = np.eye(4, dtype=float)
        e_T[:3, :3] = T_e[:3, :3].T
        e_T[:3, 3] = -T_e[:3, :3].T @ T_e[:3, 3]

        e_T_g = T_g @ e_T

        v = (e_T_g[:3, :3].trace() - 1) / 2
        theta = 0
        omega = np.zeros(3, dtype=float)

        if -1 <= v <= 1:
            theta = np.arccos(v)

        else:
            if abs(v - 1) < 1e-8:
                v = 1
            if abs(v + 1) < 1e-8:
                v = -1
            theta = np.arccos(v)

        if theta != 0:
            omega[0] = 1 / 2 / np.sin(theta) * (e_T_g[2, 1] - e_T_g[1, 2])
            omega[1] = 1 / 2 / np.sin(theta) * (e_T_g[0, 2] - e_T_g[2, 0])
            omega[2] = 1 / 2 / np.sin(theta) * (e_T_g[1, 0] - e_T_g[0, 1])

        delta_o = omega * np.sin(theta)
        #delta_o = omega * theta
        delta_p = e_T_g[:3, 3]

        delta_x = np.concatenate([delta_p, delta_o])

        x_gain = 5.
        x_dot = delta_x * x_gain
        v_e = x_dot
        
        J = np.zeros((6, manipulator_params.shape[0]), dtype=float)
        z_0 = np.array([0, 0, 1], dtype=float)
        p_tilde_0 = np.array([0, 0, 0, 1], dtype=float)

        p_tilde_e = T_e @ p_tilde_0
        p_e = p_tilde_e[:3]

        for i, params in enumerate(manipulator_params):
            if i > 0:
                T_i = np.eye(4, dtype=float)
                for j in range(i):
                    T_i = T_i @ DH(manipulator_params[j])
                z_i_minus_1 = T_i[:3, :3] @ z_0
                p_tilde_i_minus_1 = T_i @ p_tilde_0
                p_i_minus_1 = p_tilde_i_minus_1[:3]
                if i != 3:
                    J[:3, i - 1] = np.cross(z_i_minus_1, p_e - p_i_minus_1)
                    J[3:6, i - 1] = z_i_minus_1
                else :
                    J[:3, i - 1] = z_i_minus_1

        q_dot = np.linalg.pinv(J) @ v_e
        
        #rcond: rcond * largest_singular_value
        #J_inv = np.linalg.pinv(J)
        #q_dot = J_inv @ x_dot + (np.eye(8, dtype=float) - np.dot(J_inv, J)) @ q_dot_0
        
        for k, j_info in enumerate(joint_infos):
            i = int(j_info[2])
            j = int(j_info[3])
            q_dot[k] = np.sign(q_dot[k]) * min(j_info[4], abs(q_dot[k]))
            new_value = manipulator_params[i, j] + q_dot[k] * dt
            if j_info[0] <= new_value <= j_info[1]:
                manipulator_params[i, j] = new_value

        update_plot()



    while True:
        if plt.waitforbuttonpress():
            break
