import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Wedge, Circle

from shared import R, i_R_r_array, r_R_i_array
from sampler import sampler

r_from = -3
r_to = 3
def init_plot():
    plt.ion()
    fig, ax = plt.subplots(1)

    ax.set_xlim(r_from, r_to)
    ax.set_ylim(r_from, r_to)
    ax.set_aspect("equal")
    ax.set_xticks(np.arange(r_from, r_to, 0.5))
    ax.set_yticks(np.arange(r_from, r_to, 0.5))
    ax.set_axisbelow(True)
    plt.grid()
    ax.set_xlabel('X')
    ax.set_ylabel('Y')

    return fig, ax


# materijalna točka + orijentacija
class Robot:
    def __init__(self, i_xi=np.array([0, 0, 0], dtype=float)):
        self.i_xi = i_xi

    def update_state(self, i_xi_dot, dt):
        self.i_xi += i_xi_dot * dt
        self.i_xi[2] = np.arctan2(np.sin(self.i_xi[2]), np.cos(self.i_xi[2]))

    def update_state_r(self, r_xi_dot, dt):
        i_xi_dot = R(self.i_xi[2]).transpose() @ r_xi_dot
        self.update_state(i_xi_dot, dt)



if __name__ == "__main__":
    fig, ax = init_plot()

    dt = 0.01
    r_v_dot = 0.6
    r_omega = np.pi

    r = Robot(np.array([1.7, 1.5, 4 / 3 * np.pi]))
    robot_patch = ax.add_patch(
        Wedge(
            (r.i_xi[0], r.i_xi[1]),
            0.2,
            np.rad2deg(r.i_xi[2]) + 10,
            np.rad2deg(r.i_xi[2]) - 10,
        )
    )

    landmarks = np.random.rand(7, 2)
    landmarks_phi = np.arctan2(landmarks[:, 1], landmarks[:, 0])
    for landmark in landmarks:
        ax.add_patch(
            Circle(xy=landmark, radius=0.1, facecolor="#f4511e88")
        )


    M = 1000
    particles = np.random.randn(M, 3)
    particles[:, 2] = np.arctan2(np.sin(particles[:, 2]), np.cos(particles[:, 2]))
    particles_scatter = ax.scatter(particles[:, 0], particles[:, 1], s=0.5, c="#ff1744")

    plt.show()

    var = 0.01

    mean = 1 / M * np.sum(particles, axis=0)
    mean[2] = np.arctan2(np.sin(mean[2]), np.cos(mean[2]))
    er = Robot(mean)
    estimated_robot_patch = ax.add_patch(
        Wedge(
            (er.i_xi[0], er.i_xi[1]),
            0.2,
            np.rad2deg(er.i_xi[2]) + 10,
            np.rad2deg(er.i_xi[2]) - 10,
            facecolor="#00ff0088"
        )
    )

    for i in range(1000):
        r_vel = np.array([r_v_dot, 0, r_omega], dtype=float)
        r.update_state_r(r_vel, dt)

        particles += np.tensordot(
            r_vel,
            r_R_i_array(particles[:, 2]),
            axes=(0, 1)
        ) * dt
        particles += np.random.normal(0, var, (M, 3))
        particles[:, 2] = np.arctan2(np.sin(particles[:, 2]), np.cos(particles[:, 2]))

        particles_scatter.set_offsets(particles[:, :2])

        robot_patch.center = r.i_xi[0], r.i_xi[1]
        robot_patch.theta1 = np.rad2deg(r.i_xi[2]) + 10
        robot_patch.theta2 = np.rad2deg(r.i_xi[2]) - 10
        robot_patch._recompute_path()

        m_res = landmarks - r.i_xi[:2]
        m_res += np.random.normal(0, var, (landmarks.shape[0], 2))
        phi_res = landmarks_phi - r.i_xi[2]
        phi_res += np.random.normal(0, var, landmarks.shape[0])

        particles_reshaped = np.reshape(particles, (M, 1, 3))
        p_m_res = landmarks - particles_reshaped[:, :, :2]
        p_m_res += np.random.normal(0, var, (M, landmarks.shape[0], 2))
        p_phi_res = landmarks_phi - particles_reshaped[:, :, 2]
        p_phi_res += np.random.normal(0, var, (M, landmarks.shape[0]))

        diff_pos_r_p = np.linalg.norm(m_res - p_m_res, axis=(1, 2))
        diff_or_r_p = np.linalg.norm(phi_res - p_phi_res, axis=1)

        diff = diff_pos_r_p + diff_or_r_p
        x_0 = y_1 = np.min(diff)
        x_1 = y_0 = np.max(diff)
        sim = (y_1 - y_0) / (x_1 - x_0) * (diff - x_0) + y_0
        sim /= np.sum(sim)
        weights = sim
        indices = sampler(weights)
        particles = particles[indices]

        mean = 1 / M * np.sum(particles, axis=0)
        mean[2] = np.arctan2(np.sin(mean[2]), np.cos(mean[2]))
        er.i_xi = mean

        estimated_robot_patch.center = er.i_xi[0], er.i_xi[1]
        estimated_robot_patch.theta1 = np.rad2deg(er.i_xi[2]) + 10
        estimated_robot_patch.theta2 = np.rad2deg(er.i_xi[2]) - 10
        estimated_robot_patch._recompute_path()

        if i % 100 == 0:
            r_omega *= -1

        plt.pause(dt)
        fig.canvas.draw_idle()

