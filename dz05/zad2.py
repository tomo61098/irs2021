import numpy as np
import matplotlib.pyplot as plt

from shared import R, i_R_r_array, r_R_i_array
from robot import Robot
from plot2 import Plot

def g(x, u, dt):
    return x + dt * np.array([ np.cos(x[2]) * u[0], np.sin(x[2]) * u[0], u[1] ])

def G(x, u, dt):
    return np.array([[1, 0, -u[0] * dt * np.sin(x[2]) ], [0, 1, u[0] * dt * np.cos(x[2]) ], [0, 0, 1]])

x0 = np.array([ .7, 1.5, 1.5 * np.pi ])
mu0 = np.zeros(3)
sig0 = np.diag([ 1, 2, 3 ]) * 10

var1 = .05
var2 = .01

R = np.eye(3) * var1
Q = np.eye(3) * var2

dt = 0.01
r_v_dot = 0.6
r_omega = np.pi
    
R_d = R * dt
Q_d = Q / dt



def extended_kalman(mu, sig, ut, zt, dt):
    mit = g(mu, ut, dt)
    GT = G(mu, ut, dt)
    sugt = GT @ sig @ GT.T + R_d
    K_t = sugt @ np.linalg.inv(sugt + Q_d)
    mut = mit + K_t @ (zt - mit)
    sigt = (np.eye(3) - K_t) @ sugt
    return mut, sigt

if __name__ == "__main__":

    
    r = Robot(x0)
    
    plt.show()

    landmarks = []

    var = 0.01

    er = Robot(mu0)

    plot = Plot(r, er)

    for i in range(1000):
        ut = np.array([ r_v_dot, r_omega ])
        [lp, rp] = r.r_inverse_kinematics(r_v_dot, r_omega) + np.random.normal(0, var2, 2)
        r.update_state(r.forward_kinematics(lp ,rp), dt)

        zt = r.i_xi + np.random.normal(0, var1, 3)
        [er.i_xi, sig0] = extended_kalman(er.i_xi, sig0, ut, zt, dt)
        plot.update()

        if i % 100 == 0:
            r_omega *= -1

        plt.pause(dt)
