import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle, Circle, Polygon
import numpy as np


class Plot(object):
    def __init__(self, robot, estimated_robot, r_from = -3., r_to = 3.):
        plt.ion()
        self.fig = plt.figure()
        self.ax = self.fig.add_subplot(111)
        self.ax.set_xlim(r_from, r_to)
        self.ax.set_ylim(r_from, r_to)
        self.ax.set_aspect("equal")
        self.ax.set_xticks(np.arange(r_from, r_to, 0.5))
        self.ax.set_yticks(np.arange(r_from, r_to, 0.5))
        plt.grid()
        self.ax.set_xlabel('X')
        self.ax.set_ylabel('Y')

        self.robot = robot
        self.chassis_patch = self.ax.add_patch(Polygon(self.robot.chassis[:, :2], closed=True, facecolor="#009688"))
        self.wheel_l_patch = self.ax.add_patch(Polygon(self.robot.wheel_l[:, :2], closed=True, facecolor="#212121"))
        self.wheel_r_patch = self.ax.add_patch(Polygon(self.robot.wheel_r[:, :2], closed=True, facecolor="#212121"))

        self.estimated_robot = estimated_robot
        self.estimated_chassis_patch = self.ax.add_patch(Polygon(self.estimated_robot.chassis[:, :2], closed=True, facecolor="red"))
        self.estimated_wheel_l_patch = self.ax.add_patch(Polygon(self.estimated_robot.wheel_l[:, :2], closed=True, facecolor="red"))
        self.estimated_wheel_r_patch = self.ax.add_patch(Polygon(self.estimated_robot.wheel_r[:, :2], closed=True, facecolor="red"))

    def update(self):
        self.chassis_patch.xy = self.robot.chassis[:, :2]
        self.wheel_l_patch.xy = self.robot.wheel_l[:, :2]
        self.wheel_r_patch.xy = self.robot.wheel_r[:, :2]
        
        self.estimated_chassis_patch.xy = self.estimated_robot.chassis[:, :2]
        self.estimated_wheel_l_patch.xy = self.estimated_robot.wheel_l[:, :2]
        self.estimated_wheel_r_patch.xy = self.estimated_robot.wheel_r[:, :2]

        self.fig.canvas.draw_idle()

