import numpy as np
from shared import *
from sampler import sampler

def prediction(particles, r_v_dot, r_omega, dt, var):
    r_vel = np.array([ r_v_dot, 0, r_omega ], dtype = float)
    particles += np.tensordot(
            r_vel,
            r_R_i_array(particles[:, 2]),
            axes = (0, 1)
        ) * dt

    particles += np.random.normal(0, var, particles.shape)
    particles[:, 2] = np.arctan2(np.sin(particles[:, 2]), np.cos(particles[:, 2]))
    return particles

def correction(robot, landmarks, particles, var):
    M = len(particles)
    landmarks_phi = np.arctan2(landmarks[:, 1], landmarks[:, 0])

    m_res = landmarks - robot.i_xi[:2]
    m_res += np.random.normal(0, var, (landmarks.shape[0], 2))
    phi_res = landmarks_phi - robot.i_xi[2]
    phi_res += np.random.normal(0, var, landmarks.shape[0])
        
    particles_reshaped = particles[:, None]
    p_m_res = landmarks - particles_reshaped[:, :, :2]
    p_m_res += np.random.normal(0, var, (M, landmarks.shape[0], 2))
    p_phi_res = landmarks_phi - particles_reshaped[:, :, 2]
    p_phi_res += np.random.normal(0, var, (M, landmarks.shape[0]))

    diff_pos_r_p = np.linalg.norm(m_res - p_m_res, axis=(1, 2))
    diff_or_r_p = np.linalg.norm(phi_res - p_phi_res, axis=1)

    diff = diff_pos_r_p + diff_or_r_p
    x_0 = y_1 = np.min(diff)
    x_1 = y_0 = np.max(diff)
    sim = (y_1 - y_0) / (x_1 - x_0) * (diff - x_0) + y_0
    sim /= np.sum(sim)
    weights = sim

    mean = 1 / M * np.sum(particles, axis=0)
    mean[2] = np.arctan2(np.sin(mean[2]), np.cos(mean[2]))
    
    indices = sampler(weights)
    particles = particles[indices]
    return mean, particles
